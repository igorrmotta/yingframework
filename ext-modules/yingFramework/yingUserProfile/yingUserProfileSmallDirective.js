"use strict";

angular.module('yingFramework').directive('yingUserProfileSmall', function () {
    return {
        templateUrl: 'ext-modules/yingFramework/yingUserProfile/yingUserProfileSmallTemplate.html'
    }
});