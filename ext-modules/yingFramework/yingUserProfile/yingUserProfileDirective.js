"use strict";

angular.module('yingFramework').directive('yingUserProfile', function () {
    return {
        templateUrl: 'ext-modules/yingFramework/yingUserProfile/yingUserProfileTemplate.html'
    }
});