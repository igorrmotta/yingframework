"use strict";

angular.module('app').factory('dataService', ['$timeout', function ($timeout) {
    var locations = [
        {
            id: 1000,
            name: 'Raquette River',
            temperature: 55,
            guides: 20,
            rafts: 18,
            vests: 200,
            image: 'river1.jpg'
        },
        {
            id: 1001,
            name: 'Black River',
            temperature: 53,
            guides: 3,
            rafts: 22,
            vests: 250,
            image: 'river2.jpg'
        },
        {
            id: 1002,
            name: 'Black Creek',
            temperature: 34,
            guides: 22,
            rafts: 12,
            vests: 230,
            image: 'river3.jpg'
        },
        {
            id: 1003,
            name: 'Batten kill',
            temperature: 54,
            guides: 20,
            rafts: 24,
            vests: 420,
            image: 'river4.jpg'
        },
        {
            id: 1004,
            name: 'Ausable River',
            temperature: 38,
            guides: 12,
            rafts: 8,
            vests: 250,
            image: 'river5.jpg'
        }
    ];


    var employees = [
        {
            id: 5000,
            name: 'Andy Catterton',
            location: 'Raquette River',
            image: 'employee-andy.png'
        },
        {
            id: 5001,
            name: 'April Donaldson',
            location: 'Saranac River',
            image: 'employee-april.png'
        },
        {
            id: 5002,
            name: 'Don Morgan',
            location: 'Black Creek',
            image: 'employee-don.png'
        }
    ];

    var getEmployees = function () {
        return $timeout(function () {
            return employees;
        }, 500);
    };

    var getEmployee = function (id) {
        return $timeout(function () {
            for (var i = 0; i < employees.length; i++)
                if (employees[i].id = id)
                    return employees[i];
            return undefined;
        }, 300);
    };

    var getLocations = function () {
        return $timeout(function () {
            return locations;
        }, 500);
    };

    var getLocation = function (id) {
        var timeout = $timeout(function () {
            //Uncomment to test error on widgets
            //$timeout.cancel(timeout);
            //return undefined;
            for (var i = 0; i < locations.length; i++)
                if (locations[i].id = id)
                    return locations[i];
            return undefined;
        }, 300);
        return timeout;
    };

    return {
        getLocations: getLocations,
        getLocation: getLocation,
        getEmployees: getEmployees,
        getEmployee: getEmployee
    }
}]);